package io.swsb;

import org.jboss.resteasy.plugins.server.tjws.PatchedHttpServletRequest;

import javax.ws.rs.core.Context;

/**
 * Created by swsb.
 */
public class HelloWorld
{
    /**
     * We should put markdown here for a pretty message
     *
     * That would be kind of cool I guess
     *
     * @param foo - Http Request
     * @param req - Request Body
     * @return A Generic Object
     */
    @RestDoc(
            description = "Health check service",
            headers = {},
            path = "/health",
            httpResponses = {
                    @HttpResponse(statusCode = 200, reason = "Service is alive and well"),
                    @HttpResponse(statusCode = 503, reason = "Service is not available")
            },
            method = HttpMethod.GET,
            name = "Health Service",
            requestBody = User.class
    )
    public Object foo(Object foo, @Context PatchedHttpServletRequest req)
    {
        return new Object();
    }
}
